# Unofficial students repo for Informatics notes

From the students, to the students

## Clone this repo!

Use [git](https://git-scm.com/) to download this project.

```bash
git clone https://gitlab.com/grisus/usi.git
```

## Usage
Every user is responsible for their own behaviour, we don't assume any responsibility for improper use of this project.

Paying attention to class ["Reason and Responsibility in Decision Making"](https://www.icorsi.ch/course/view.php?id=14671) could result very useful if you are not sure about the way you should behave.

> "Two things fill the mind with ever new and increasing admiration and awe, the more often and steadily we reflect upon them: the starry heavens above me and the moral law within me."

\- Immanuel Kant, The Critique of Practical Reason, 1788


## Contributing
Push requests are welcome. 

Feel free to contribute to the project the way you prefer, and stick to the moral rules to let everybody enjoy the project.

Always think before acting: all changes are monitored (that's the only purpose of a subversioning system) and misbehaving could result in terrible consequences

## Contact Us
Any doubt or info? [Write an email!](mailto:guidel@usi.ch)


## License
[GPL](https://choosealicense.com/licenses/gpl-2.0/)