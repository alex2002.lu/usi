# Structured Computer Organization

- Explain the difference between the translation and interpretation of instructions
- Explain the key characteristics of the digital logic level, the microarchitecture level, the ISA level, and the operating system machine level
- Explain the boundary between software and hardware, given the above levels
- Explain how the terms "level", "abstraction", and "virtual machine" relate
- Explain the structure of the "Von Neumann Machine"