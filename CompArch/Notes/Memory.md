# Memory
The [[Secondary Memory]], or main memory, has the job of storing quickly accessible information for the [[CPU]]

The [[Memory]] , or main memory, holds instructions and data when a program is exectuting by the [[CPU]], whle auxiliary memory, or [[Secondary Memory]], holds data and programs not currently in use and provides longterm storage.


There are also 2 main types of memory: RAM and ROM
![[primary_mem.png]]
 
The characteristics of RAM are:
-   It is volatile
-   You can read and write to it
-   It is quicker to access than secondary storage
-   It has the largest capacity of all main memory

The characteristics of ROM are:
-   It is non-volatile
-   It is written by the computer manufacturer
-   Usually stores the BIOS
-   Smaller capacity than RAM


### A comparison between the two
RAM has a much, much greater capacity than ROM. RAM is used to hold the data and instructions that are being executed, such as the operating system and any programs you are using. When you insert a disc into a games console, the code on the disc won't be executed until it has been copied from the disc into the system RAM, which is why you see a loading screen.

As ROM is read-only memory, it tends to store core software instructions that don’t need to be changed, such as the code needed to load the operating system into RAM or the BIOS.

The table below summarises the comparisons of these two memory devices.
![[RAM&ROM.png]]


