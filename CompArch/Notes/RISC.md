# RISC
## Reduced Instruction Set Computer (RISC)
Their argument was that even if a RISC machine takes four or five instructions to do what a [[CISC]] machine does in one [[ISA]], if the RISC instructions are 10 times as fast (because they are not interpreted), RISC wins. It is also worth pointing out that by this time the speed of main [[Memory]] had caught up to the speed of readonly control stores, so the interpretation penalty had greatly increased, strongly favoring RISC machines.

A sub-category of RISC is ARM (Acorn RISC Machine), their main advantage against CISC is that they are greatly power efficient 