# The CPU
The Central Processing Unit (CPU) is the ‘‘brain’’ of the computer. Its function is to execute programs stored in the main [[memory]] by fetching their instructions, examining them, and then executing them one after another. The components are connected by a bus, which is a collection of parallel wires for transmitting address, data, and control signals. Buses can be external to the [[CPU]], connecting it to memory and [[IO]] devices, but also internal to the [[CPU]].

The CPU also contains a small, high-speed [[memory]] used to store temporary results and certain control information. This memory is made up of a number of [[Register]] s, each having has a certain size and function.


[[RISC]] vs [[CISC]]

[[RISC]] tries to use many simpler [[ISA]] in more cycles but faster by allocating more [[Register]] for an instruction, while [[CISC]] uses one [[ISA]] in one [[Register]] but with an higer complexity level 

Related to the concept of parallelism Multiprocessors can be considered as "hardware level" parallelism for [[ISA]] as can be seen below where 
![[Multiproc.png]]
