# ALU
The Artithmetic Logic Unit (ALU) is a component of the [[CPU]] that has the main role of compute arithmetic operations such as additions, multiplications, subtractions, divisions and other operations outputing us a result in the [[Register]] which can be later on be written on the [[Memory]]. 

![[datapathalu.png]]

Most instructions can be divided into one of two categories: R-M
or R-R. R-M instructions allow memory words to be fetched into registers, where, for example, they can be used as [[ALU]] inputs in subsequent instructions.

The other kind of instruction is R-R. A typical register-register instruction fetches two operands from the registers, brings them to the ALU input registers, performs some operation on them (such as addition or [[Boolean operation]] AND), and stores the result back in one of the registers. The process of running two operands through the ALU and storing the result is called the data path cycle and isthe heart of most [[CPU]]s.