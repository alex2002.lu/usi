# Von Neumann Machine

![[VonNeumannMachine.png]]

The Von Neumann Machine in the aggregations of the [[ALU]], [[Memory]], [[Control Unit]], [[Register]], [[CPU]] and [[IO]]. All those components are connected via what are called _data paths_.   

