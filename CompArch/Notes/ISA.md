# ISA 
 
ISA also known as Instruction Set Architecture is an expression of instruction given to the [[CPU]] 

## Instruction Execution
The [[CPU]] executes each instruction in a series of small steps. Roughly speaking, the steps are as follows:
1. Fetch the next instruction from [[memory]] into the instruction [[Register]].
2. Change the program counter to point to the following instruction.
3. Determine the type of instruction just fetched.
4. If the instruction uses a word in [[memory]], determine where it is.
5. Fetch the word, if needed, into a CPU [[Register]].
6. Execute the instruction.
7. Go to step 1 to begin executing the following instruction.

This sequence of steps is frequently referred to as the fetch-decode-execute cycle. It is central to the operation of all computers and are exectuted by the [[Register]]

## Design Principles for Modern Computers

### 1. All Instructions Are Directly Executed by Hardware
All common ISA are directly executed by the hardware. They are not interpreted by microinstructions. Eliminating a level of interpretation provides high speed for most instructions. For computers that implement [[CISC]] instruction sets, the more complex instructions may be broken into separate parts, which can then be executed as a sequence of microinstructions. This extra step slows the machine down, but for less frequently occurring instructions it may be acceptable.

### 2. Maximize the Rate at Which Instructions Are Issued
Modern computers resort to many tricks to maximize their performance, chief among which is trying to start as many instructions per second as possible.

### 3. Instructions Should Be Easy to Decode
That includes making instructions regular, of fixed length, and with a
small number of fields. The fewer different formats for instructions, the better.

### 4. Only Loads and Stores Should Reference Memory
One of the simplest ways to break operations into separate steps is to require
that operands for most instructions come from—and return to—CPU registers.

### 5. Provide Plenty of Registers
Since accessing [[Memory]] is relatively slow, many [[Register]] (at least 32) need to be provided, so that once a word is fetched, it can be kept in a register until it is no longer needed. 

## Parallelism
Computer architects are constantly striving to improve performance of the machines they design. Making the chips run faster by increasing their clock speed is one way, but there is a limit to what is possible by brute force. Consequently, most computer architects look to parallelism (doing two or more things at once) as a way to get even more performance for a given clock speed.

### Pipelining 
Pipelining is the process of having instructions stored in a special set of registers called the prefetch buffer. This way, when an instruction was needed, it could usually be taken from the _prefetch buffer_ rather than waiting for a memory read to complete.

Parallelism comes in two flavours: Instruction-level and Processor-level
![[5s_pipeline.png]]

Pipelining allows a trade-off between latency (how long it takes to execute an
instruction), and processor bandwidth (how many MIPS the CPU has). With a
cycle time of _T nsec_, and _n_ stages in the pipeline, the latency is _nT_ nsec becauseeach instruction passes through n stages, each of which takes _T_ nsec.

#### Superscalar Architecture
A Superscalar Architecture is just as the five-stages pipeline with the only difference that the Instruction execution unit is divided in 5 units 
![[Superscalar_processor.png]]

### Processor-Level Parallelism 
A Single Instruction-stream Multiple Data-stream or SIMD processor consists of a large number of identical processors that perform the same sequence of instructions on different sets of data. 
![[SIMD.png]]


### Multiprocessors 
The processing elements in a data parallel processor are not independent
[[CPU]], since there is only one control unit shared among all of them. Our first parallel system with multiple full-blown CPUs is the multiprocessor, a system withmore than one CPU sharing a common [[Memory]]
![[Multiproc.png]]