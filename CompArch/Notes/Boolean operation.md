# Boolean Operations 
Boolean operations are calculated inside the [[ALU]] 

The operation are made using the _boolean algebra_ through what are called _logic gates_, where *A* and *B* are the inputs and *X* is the output 
![[logic_gates.png]]
With the combination of those basic gates we can make every possible arithmetic operation inside the [[CPU]]
